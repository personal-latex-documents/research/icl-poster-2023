@default_files = ('poster.tex');
$pdflatex = 'lualatex -interaction=nonstopmode';
$pdf_mode = 1;
$bibtex_use = 2;
$clean_ext = "nav snm";

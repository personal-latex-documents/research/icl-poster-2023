# Requires GNU make, xargs, a latex distribution, sage
# and sagetex.sty visible in TEXINPUTS

MAINTEXFILE = poster.tex
TEXFILES = ${MAINTEXFILE}
SAGETEXSCRIPT = poster.sagetex.sage

poster.pdf: ${TEXFILES}  poster.sagetex.sout.tmp
	latexmk

poster.sagetex.sout.tmp: ${SAGETEXSCRIPT}
	sage ${SAGETEXSCRIPT}

${SAGETEXSCRIPT}: ${TEXFILES}
	latexmk || echo this shoud fail

.PHONY: clean nosage
clean:
	rm -rf **/__pycache__
	latexmk -C
	git clean -xf || echo no git repo to use for cleaning

nosage:
	latexmk

noappendix: ${TEXFILES}  poster.sagetex.sout.tmp
	latexmk

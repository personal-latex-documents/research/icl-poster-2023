% Gemini theme
% https://github.com/anishathalye/gemini

\documentclass[15pt]{beamer}

% ====================
% Packages
% ====================

\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage[size=a0paper,scale=1.07]{beamerposter}
\usetheme{gemini}
\usecolortheme{gemini}
\usepackage{graphicx}
\usepackage{booktabs}
\usepackage{tikz}
\usepackage{pgfplots}
\pgfplotsset{compat=1.14}
\usepackage{anyfontsize}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{sagetex}
\usepackage{subcaption}
\usepackage{hyperref}
\usepackage{wrapfig}
\usepackage{tcolorbox}
\usepackage{xcolor}
\usepackage{pifont}
\usepackage{tikz}

\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\chern}{\operatorname{ch}}
\newcommand{\lcm}{\operatorname{lcm}}
\newcommand{\firsttilt}[1]{\mathcal{B}^{#1}}
\newcommand{\bddderived}{\mathcal{D}^{b}}
\newcommand{\centralcharge}{\mathcal{Z}}
\newcommand{\coh}{\operatorname{Coh}}
\newcommand{\rank}{\operatorname{rank}}
\newcommand{\degree}{\operatorname{deg}}
\newcommand{\realpart}{\mathfrak{Re}}
\newcommand{\imagpart}{\mathfrak{Im}}

% ====================
% Lengths
% ====================

% If you have N columns, choose \sepwidth and \colwidth such that
% (N+1)*\sepwidth + N*\colwidth = \paperwidth
\newlength{\sepwidth}
\newlength{\colwidth}
\setlength{\sepwidth}{0.025\paperwidth}
\setlength{\colwidth}{0.3\paperwidth}

\newcommand{\separatorcolumn}{\begin{column}{\sepwidth}\end{column}}

% ====================
% Title
% ====================

\title{TIGHTER BOUNDS ON RANKS OF TILT SEMISTABILIZERS}

\author{Practical methods to narrow down possible walls for Bridgeland
stabilities for complexes of sheaves on Picard rank 1 surfaces.}

\institute[shortinst]{Luke Naylor}

% ====================
% Footer (optional)
% ====================

\footercontent{
  %\href{https://www.example.com}{https://www.example.com} \hfill
  \hfill
  Derived Categories, Moduli Spaces, and Counting Invariants,
  ICL, London \hfill
  \href{mailto:l.naylor@sms.ed.ac.uk}{l.naylor@sms.ed.ac.uk}}
% (can be left out to remove footer)

% ====================
% Logo (optional)
% ====================

% use this to include logos on the left and/or right side of the header:
\logoright{\includegraphics[height=7cm]{qr_code.png}}
\logoleft{\includegraphics[height=7cm]{UoE_Logo_White.png}}

% ====================
% Body
% ====================

\begin{document}

\begin{sagesilent}
from pseudowalls import *
Δ = lambda v: v.Q_tilt()
mu = stability.Mumford().slope

def beta_minus(v):
  beta = stability.Tilt().beta
  solutions = solve(
    stability.Tilt(alpha=0).degree(v)==0,
    beta)
  return min(map(lambda s: s.rhs(), solutions))

def beta_plus(v):
  beta = stability.Tilt().beta
  solutions = solve(
    stability.Tilt(alpha=0).degree(v)==0,
    beta)
  return max(map(lambda s: s.rhs(), solutions))

def correct_hyperbola_intersection_plot():
  var("alpha beta", domain="real")
  coords_range = (beta, -2.5, 0.5), (alpha, 0, 2.5)
  delta2 = 1/2
  pbeta=-1.5
  text_args = {"fontsize":"large", "clip":True}
  black_text_args = {"rgbcolor":"black", **text_args}
  p = (
    implicit_plot( beta^2 - alpha^2 == 2,
        *coords_range , rgbcolor = "black", legend_label=r"a")
    + implicit_plot((beta+delta2)^2 - alpha^2 == (delta2-2)^2-2,
        *coords_range , rgbcolor = "green")
    + point([-2, sqrt(2)], size=50, rgbcolor="black", zorder=50)
    + text("Q",[-2, sqrt(2)+0.1], **black_text_args)
    + point([pbeta, sqrt(pbeta^2-2)], size=50, rgbcolor="black", zorder=50)
    + text("P",[pbeta+0.1, sqrt(pbeta^2-2)], **black_text_args)
    + circle((-2,0),sqrt(2), linestyle="dashed", rgbcolor="purple")
    # dummy lines to add legends (circumvent bug in implicit_plot)
    + line([(2,0),(2,0)] , rgbcolor = "purple", linestyle="dotted",
        legend_label=r"pseudo-wall")
    + line([(2,0),(2,0)] , rgbcolor = "black",
        legend_label=r"$\Theta_v^-$")
    + line([(2,0),(2,0)] , rgbcolor = "green",
        legend_label=r"$\Theta_u^-$")
    # vertical characteristic lines
    + line([(0,0),(0,coords_range[1][2])],
        rgbcolor="black", linestyle="dashed",
        legend_label=r"$V_v$")
    + line([(-delta2,0),(-delta2,coords_range[1][2])],
        rgbcolor="green", linestyle="dashed",
        legend_label=r"$V_u$")
  )
  p.set_legend_options(loc="upper right", font_size="x-large",
    font_family="serif")
  p.xmax(coords_range[0][2])
  p.xmin(coords_range[0][1])
  p.ymax(coords_range[1][2])
  p.ymin(coords_range[1][1])
  p.axes_labels([r"$\beta$", r"$\alpha$"])
  return p

def charact_curves(v):
    alpha = stability.Tilt().alpha
    beta = stability.Tilt().beta
    coords_range = (beta, -4, 5), (alpha, 0, 4)
    text_args = {"fontsize":"xx-large", "clip":True}
    black_text_args = {"rgbcolor": "black", **text_args}
    p = (
      implicit_plot(stability.Tilt().degree(v), *coords_range )
      + line([(mu(v),0),(mu(v),5)], linestyle = "dashed")
      + text(r"$\Theta_v^+$",[3.5, 2], rotation=45, **text_args)
      + text(r"$V_v$", [0.43, 1.5], rotation=90, **text_args)
      + text(r"$\Theta_v^-$", [-2.2, 2], rotation=-45, **text_args)
      + text(r"$\nu_{\alpha, \beta}(v)>0$", [-3, 1], **black_text_args)
      + text(r"$\nu_{\alpha, \beta}(v)<0$", [-1, 3], **black_text_args)
      + text(r"$\nu_{\alpha, \beta}(-v)>0$", [2, 3], **black_text_args)
      + text(r"$\nu_{\alpha, \beta}(-v)<0$", [4, 1], **black_text_args)
    )
    p.xmax(5)
    p.xmin(-4)
    p.ymax(4)
    p.axes_labels([r"$\beta$", r"$\alpha$"])
    return p

v1 = Chern_Char(3, 2, -2)
v2 = Chern_Char(3, 2, 2/3)
\end{sagesilent}

\begin{frame}[t]
\begin{columns}[t]
\separatorcolumn

\begin{column}{\colwidth}
  \begin{block}{Introduction}
    The theory of Bridgeland stability conditions on complexes of sheaves was
    developed as a generalisation of stability of vector bundles. The
    definition is most analoguous to Mumford stability, but is more aware of the
    features that sheaves can have on spaces of dimension greater than 1. Whilst
    also asymptotically matching up with Gieseker stability.

    There is work done to study the way that the moduli spaces of stable objects
    of some fixed Chern character $v$ changes as we vary the Bridgeland
    stability conditions. They in fact do not change over whole regions of the
    stability space (call chambers), but do undergo changes as we cross `walls'
    in the stability space. These are where there is some stable object $F$ of Chern
    character $v$ which has a subobject who's slope overtakes the slope of $v$,
    making $E$ unstable over the wall.
  \end{block}

  \begin{block}{Tilt Stability on Picard Rank 1 Surfaces}
    \heading{Picard rank 1}
    For the surfaces ($X$) considered, there's a fixed ample line bundle $L$
    which generates the Neron-Severi group. This way, taking $\ell:=c_1(L)$, any
    Chern character of an element in $\bddderived(X)$ can be written
    $(r,c\ell,d\ell^2/2)$, with integer coordinates $r,c,d$. The product and sum
    laws are then entirely determined by these coordinates.
    \heading{Tilt Stability}
    Tilt stabilities are Bridgeland stability conditions on $\bddderived(X)$,
    where stability is decided among objects of a heart, constructed by tilting
    $\coh(X)$ using some torsion theories.
    Stability is decided by comparing complex arguments of a central
    charge $\centralcharge$ mapping this heart into
    $\{z\in\CC\colon\imagpart(z)>0\}\cup\RR_{\leq0}$.
    Or equivalently, By comparing slopes, just as in Mumford stability, given by
    $\nu=\frac{\realpart(\centralcharge)}{\imagpart(\centralcharge)}$


    In this class of surfaces, the tilt stabilities
    $\{\sigma_{\alpha,\beta} \colon \alpha \in \QQ_{>0}, \beta \in \QQ\}$
    in question are defined with central charge
    $\centralcharge_{\alpha,\beta}(v):=\left<v, \exp(-\beta\ell-i\alpha\ell)\right>$.
    With corresponding hearts for $\bddderived(X)$ depending only on $\beta$,
    looking more like $\coh(X)$ as $\beta\to-\infty$.

    \heading{Connection to Gieseker Stability}
    \begin{columns}
      \begin{column}{0.6\linewidth}
        $\beta \to -\infty$, Fixed $\alpha$,
        \begin{equation*}
          \nu_{\alpha, -n}(E) =
          \frac{
            \chern_2(E\otimes L^n)
            {\color{gray}
            - \frac{\alpha^2}{2} \rank(E)
            }
          }{
            {\color{gray}
            \chern_1(E) +
            }
            n \rank(E)
          }
          \qquad
          n \in \ZZ
        \end{equation*}
        $\rightsquigarrow$ $\sigma_{\alpha,\beta}$ tends to Gieseker Stability
      \end{column}
      \begin{column}{0.39\linewidth}
         \begin{tcolorbox}[title=Gieseker Stability]
            E stable when red. Hilb. poly.
            \[
              p_E(n) = \frac{\chern_2(E\otimes L^n)}{\rank(E)}
            \]
            not overtaken by $p_F(n)$ of any
            $0 \not= F \hookrightarrow E$, for large $n$.
        \end{tcolorbox}
      \end{column}
    \end{columns}
  \end{block}

  \begin{alertblock}{Goals}
    \begin{columns}[t]
      \begin{column}{.49\linewidth}
        \heading{This Project}
        \begin{itemize}
          \item Improve of Benjamin Schmidt's explicit bound for ranks of tilt
            semistabilizers \textbf{(middle column)}
          \item Develop faster tool to narrow down possible tilt walls,
            when finitely many ($\beta_{-}\in\QQ$),
            to later filter through by hand with geometric knowledge
            \textbf{(right column)}
        \end{itemize}
      \end{column}
      \begin{column}{.50\linewidth}
        \heading{Future Applications}
        \begin{itemize}
          \item Use theory from \cite{yanagida2014bridgeland} to give more
            meaningful results on abelian surfaces when there are infinitely
            many
          \item Use theory from \cite{JardimMarcos2019Waaf} and
            \cite{BogGiestypeineq} to link these result to stabilities on
            3-folds with an analogue for $\lambda$-walls intersecting $\Theta_v$
        \end{itemize}
      \end{column}
    \end{columns}
  \end{alertblock}

\end{column}

\separatorcolumn

\begin{column}{\colwidth}

  \begin{block}{Characteristic Curves}
    For a given Chern character $v$ with positive rank and $\Delta(v) \geq 0$
    there are two characteristic curves on the stability conditions.


    \begin{columns}[t]
      \begin{column}{0.49\linewidth}
        \begin{equation*}
          V_v \colon \imagpart\centralcharge(v) = 0 \qquad
          \Theta_v \colon \realpart\centralcharge(v) = 0
        \end{equation*}
        \begin{figure}
        \begin{tikzpicture}
          \node[anchor=south west, inner sep=0] (X) at (0,0){
            \sageplot[width=\linewidth]{charact_curves(v1)}
          };%
          \begin{scope}[x={(X.south east)},y={(X.north west)}]%
          \node[text=blue] (Z) at (0.43,0.13) {%
            \small{
            $\beta_{-}$
            }
          };
          \node[text=blue] (Z) at (0.55,0.13) {%
            \small{
            $\mu(v)$
            }
          };
          \node[text=blue] (Z) at (0.67,0.13) {%
            \small{
            $\beta_{+}$
            }
          };
          \end{scope}
        \end{tikzpicture}
        \label{fig:charact_curves_vis}
        \end{figure}
      \end{column}

      \begin{column}{0.49\linewidth}
        \begin{itemize}
          \item $V_v$ is a vertical line at $\beta=\mu(v)$
          \item $\Theta_v$ is oriented with left-right branches (as opposed to up-down).
          \item The base points of $\Theta_v$ are at
            $\beta=\beta_{\pm}:=\mu(v)\pm\frac{\sqrt{\Delta(v)}}{\rank(v)}$
          \item Walls other than $V_v$ are circles with centre on the $\beta$-axis
            and maximum on $\Theta_v$
        \end{itemize}
      \end{column}
    \end{columns}
  \end{block}

  \begin{block}{Loose Bound on Semistabilizer Ranks - Benjamin Schmidt
    \cite{SchmidtBenjamin2020Bsot}}
    Given a Chern character $v$ such that $\beta_-:=\beta_{-}(v)\in\QQ$, the
    \textbf{rank $r$} of any \textbf{semistabilizer} $E$ of some $F \in
    \firsttilt{\beta_-}$ with $\chern(F)=v$ is bounded above by:

    \begin{equation*}
      r \leq \frac{mn^2 \chern^{\beta_-}_1(v)^2}{\gcd(m,2n^2)}
    \end{equation*}
  \end{block}

  \begin{alertblock}{Convenient Bound on Ranks of Semistabilizers
    \cite{NaylorDoc2023}}
    \bgroup
    \let\originalbeta\beta
    \renewcommand\beta{{\originalbeta_{-}}}
      Let $v$ be a fixed Chern character and
      $R:=\chern_0(v) \leq n^2\Delta(v)$.
      Then the \textbf{ranks} of the pseudo-semistabilizers for $v$
      are \textbf{bounded above} by the following expression.

      \begin{equation*}
        \frac{1}{2}R
        + \frac{\Delta(v)n^2}{4\ell^2}
        + \frac{R^2\ell^2}{4\Delta(v)n^2}
      \end{equation*}
    \egroup
    \begin{flushright}
    \textbf{Up to a quarter of previous bound}
    \end{flushright}
  \end{alertblock}

  \begin{exampleblock}{Stronger Bound on Ranks of Semistabilizers
    \cite{NaylorDoc2023}}
    \bgroup
    \let\originalbeta\beta
    \renewcommand\beta{{\originalbeta_{-}}}
    \def\kappa{k_{v,q}}
    \def\psi{\chern_1^{\beta}(F)}
    \renewcommand{\aa}{{a_v}}
    \newcommand{\bb}{{b_q}}
    Let $v$ be a fixed Chern character, with $\frac{a_v}{n}=\beta:=\beta(v)$
    rational and expressed in lowest terms.
    Then the \textbf{ranks $r$} of the pseudo-semistabilizers $u$ for $v$ with
    $\chern_1^\beta(u) = q = \frac{b_q}{n}$
    are \textbf{bounded above} by the following expression:
    \begin{align*}
      \min
      \left(
        \frac{n^2q^2}{\kappa}, \:\:
        \frac{n^2\left(\chern_1^\beta(v)-q\right)^2}{\kappa} + R\:\:
      \right)
    \end{align*}

    Where $\kappa$ is the least $k\in \ZZ_{>0}$ satisfying
    $k \equiv -\aa\bb \pmod{n}$,
    and $R = \chern_0(v)$

    Furthermore, if $\aa \not= 0$ then
    $\aa r \equiv b_q \pmod{n}$.
    \egroup

    \begin{flushright}
    \textbf{Potentially much tighter bound, but relies on specific
    $\chern_1^{\beta_{-}}(u)$ values}

    \textbf{More involved calculation better suited to computer program}
    \end{flushright}
  \end{exampleblock}

\end{column}

\separatorcolumn

\begin{column}{\colwidth}

  \begin{block}{Numerical Tests for left-wall Pseudo-semistabilizers
    \cite{NaylorDoc2023}}
    Let $v$ and $u$ be Chern characters with positive ranks and $\Delta(v),
    \Delta(u)\geq 0$. Let $P$ be a point on $\Theta_v^-$.

    \begin{wrapfigure}{r}{0.5\textwidth}
    \sageplot[width=\linewidth]{correct_hyperbola_intersection_plot()}
    \caption{Configuration of characteristic curves for $u$ and $v$ satisfying
    these conditions}
    \end{wrapfigure}

    \noindent
    The following conditions together, about walls:
    \begin{itemize}
      \item $u$ gives rise to a pseudo-wall for $v$, \textit{left} of the vertical line $V_v$
      \item The pseudo-wall contains $P$ in it's \textit{interior}
      ($P$ can be chosen to be the base of the left branch to target all left-walls)
    \item $u$ destabilizes $v$ going \textit{`inwards'}:
      $\nu_{\alpha,\beta}(u)<\nu_{\alpha,\beta}(v)$ outside the pseudo-wall, and
      $\nu_{\alpha,\beta}(u)>\nu_{\alpha,\beta}(v)$ inside.
    \end{itemize}

    \noindent
    Are equivalent to the following, more numerical conditions:
    \begin{enumerate}
      \item $\beta(P)<\mu(u)<\mu(v)$, i.e., $V_u$ is strictly between $P$ and $V_v$.
      \item $\chern_2^{P}(u)>0$
    \end{enumerate}
  \end{block}


  \begin{exampleblock}{Some Pseudo-wall Finding Algorithms for Rational $\beta_{-}$}
    Fix a Chern character $v$ with positive rank, $\Delta(v)\geq0$, and
    $\beta_{-}(v)$ rational.
    The following algorithms find all Chern characters $u$ which satisfy the
    numerical conditions just above, as well as some appropriate Bogomolov
    inequalities.
    \begin{columns}[t]
      \hfill
      \begin{column}{.59\linewidth}
        \heading{B. Schmidt's Algorithm \cite{SchmidtGithub2020} (mod details)}
        \begin{itemize}
          \item Calculate upper bound $r_{max}$ for ranks of tilt
            semistabilizers $u$
          \item Iterate through possible $\frac{c}{r}$ values decreasing between
            $\mu(v)$ and $\beta_{-}$ s.t. $r\leq r_{max}$
            \begin{itemize}
              \item[\ding{111}] $\chern_0(u)=r$, $\chern_1(u)=c$ are determined
              \item[\ding{111}] Find $\chern_2(u)$ values s.t.
                $u$ satisfies appropriate inequalities
            \end{itemize}
        \end{itemize}
        \heading{Limitations}
        \begin{itemize}
          \item Bound on $\rank(u)$ not very tight
          \item Many $\frac{c}{r}$ values considered yield no solutions
            (esp. when $\frac{c}{r}$ close to $\mu(v)$ and $r$ large)
        \end{itemize}
        $\rightsquigarrow$ Occasionally very slow
      \end{column}
      \begin{column}{.39\linewidth}
        \heading{Alternative \cite{NaylorRust2023}}
        \begin{itemize}
          \item Take $\frac{a_v}{n}=\beta_{-}(v)$ in lowest terms
          \item Iterate through possible values
            $q=\frac{b_q}{n}:=\chern_1^{\beta_{-}}(u)$
            \begin{itemize}
              \item[\ding{111}] Calculate upper bound for tilt
                semistabilizers with $\chern_1^{\beta_{-}}(u)=q$
                (see "stronger bound")
              \item[\ding{111}] Iterate through $r>0$ values up to this bound
                sufficiently large that $\mu(u)<\mu(v)$ and
                $a_v r\equiv b_q \pmod{n}$
                \begin{itemize}
                  \item[\ding{109}] $\chern_0(u)$, $\chern_1(u)$ are determined
                  \item[\ding{109}] Find range of$\chern_2(u)$ values which
                    satisfy appropriate inequalities
                \end{itemize}
            \end{itemize}
        \end{itemize}
      \end{column}
      \hfill
    \end{columns}
  \end{exampleblock}

  \begin{block}{References}

    \nocite{*}
    \footnotesize{\bibliographystyle{plain}\bibliography{poster}}

  \end{block}

\end{column}

\separatorcolumn
\end{columns}
\end{frame}

\end{document}
